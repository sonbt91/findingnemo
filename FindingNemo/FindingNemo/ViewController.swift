//
//  ViewController.swift
//  FindingNemo
//
//  Created by The Son    on 7/25/16.
//  Copyright © 2016 Savvycom. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    // ! means that the variable cannot be nil (always exists)
    // ? means that the variable can be nil (exists or not)
    
    @IBOutlet var mapView:GMSMapView!
    @IBOutlet var searchTextField:SearchLocationTextField!
    @IBOutlet var routeButton:UIButton!
    
    var locationManager:CLLocationManager!
    var currentCoordinate:CLLocationCoordinate2D!
    var searchLocationViewController:SearchLocationViewController?
    var placeClient:GMSPlacesClient!
    var polyline:GMSPolyline?
    var destinationPlace:GMSPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initObjects()
        self.setupUI()
        self.registerNotifications()
    }
    
    func initObjects() -> Void {
        
        placeClient = GMSPlacesClient()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        mapView.delegate = self
    }
    
    func setupUI() -> Void {
        
        // Create the left view for search field. 
        // Set the mode is Always for keeping it always display in the field
        let leftView = UIImageView(image: UIImage(named: "location-marker-icon"))
        leftView.contentMode = .Center
        leftView.frame = CGRect(x: 0, y: 0, width: 35, height: CGRectGetHeight(searchTextField.frame))
        searchTextField.leftView = leftView
        searchTextField.leftViewMode = .Always
        
        // Right view will be the delete icon
        let deletePlaceButton = UIButton(type: .Custom)
        deletePlaceButton.frame = CGRect(x: 0, y: 0, width: 40, height: CGRectGetHeight(searchTextField.frame))
        deletePlaceButton.setImage(UIImage(named: "delete-icon"), forState: .Normal)
        deletePlaceButton.addTarget(self, action: #selector(ViewController.deleteDestinationPlace(_:)), forControlEvents: .TouchUpInside)
        searchTextField.rightView = deletePlaceButton
        searchTextField.rightViewMode = .Always
        searchTextField.rightView?.hidden = true
        searchTextField.placeholder = "Search a location"
        
        routeButton.layer.masksToBounds = true
        routeButton.layer.cornerRadius = CGRectGetHeight(routeButton.frame)/2
        routeButton.layer.masksToBounds = false
        routeButton.layer.shadowColor = UIColor.grayColor().colorWithAlphaComponent(0.7).CGColor
        routeButton.layer.shadowOpacity = 1.0
        routeButton.layer.shadowRadius = 1.5
        routeButton.layer.shadowOffset = CGSizeMake(0.25, 1.3)
        routeButton.hidden = true
    }
    
    func registerNotifications() -> Void {
        
        // Register with the OS to receive the notification when the app come to active state
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.appCameBackToActive(_:)), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        // Hide keyboard when we are about to go to the search screen
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        // Make sure that user enables location service to use the app
        self .askForLocationServicePermission()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToSearchView" {
            // The instance of SearchLocationViewController is the destination of seguge
            searchLocationViewController = segue.destinationViewController as? SearchLocationViewController
            searchLocationViewController?.searchString = searchTextField.text
            
            // Callback when user chooses a location
            searchLocationViewController?.searchLocationWithResult({ (prediction) in
                self.routeButton.hidden = false
                self.displayDestinationLocation(prediction)
            })
        }
    }
    
    func appCameBackToActive(sender:AnyObject) -> Void {
        self.askForLocationServicePermission()
    }
    
    // MARK: <-- Location service permission -->
    // This function will ask user to enable location services from the device's settings
    func askForLocationServicePermission() -> Void {
        
        if (!CLLocationManager.locationServicesEnabled()) {
            // Show an alert which asks users to turn on location services
            let alertController = UIAlertController(title: AppName, message: "Please turn on Location Services in Device Settings.", preferredStyle: .Alert)
            
            // There're 2 options
            // 1. Turn ON: If user chooses that, then bring him to device's settings
            let turnOnServiceAction = UIAlertAction(title: "Turn ON Location Services", style: .Default, handler: { (action) in
                self.goToAppSettings()
            })
            // 2. Do it later, then do nothing
            let noneAction = UIAlertAction(title: "Do it later", style: .Default, handler:nil)
            alertController.addAction(turnOnServiceAction)
            alertController.addAction(noneAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            switch CLLocationManager.authorizationStatus() {
            case .Authorized, .AuthorizedWhenInUse:
                locationManager.startUpdatingLocation()
                break
            case .NotDetermined:
                locationManager.requestWhenInUseAuthorization()
            case  .Restricted, .Denied:
                let alertController = UIAlertController(title: AppName, message: "Please enable Location Access for the app by going to: Settings > Privacy > Location Services > " + AppName + " then choose While Using the App.", preferredStyle: .Alert)
                let turnOnServiceAction = UIAlertAction(title: "Turn ON Location Services", style: .Default, handler: { (action) in
                    self.goToAppSettings()
                })
                alertController.addAction(turnOnServiceAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                break;
            }
        }
    }
    
    // MARK: <-- Hanlde destination location -->
    func displayDestinationLocation(prediction:GMSAutocompletePrediction) -> Void {
        placeClient.lookUpPlaceID(prediction.placeID!) { (place, error) in
            self.destinationPlace = place
            self.searchTextField.text = place?.name
            self.searchTextField.rightView?.hidden = false
            
            self.mapView.clear()
            let rootMarker:GMSMarker = GMSMarker(position: (place?.coordinate)!)
            rootMarker.icon = UIImage(named: "default_marker")
            rootMarker.map = self.mapView
            self.mapView.animateToLocation((place?.coordinate)!)
        }
    }
    
    // This function will do things below:
    // 1. Call Google directions API to get all routes from the current location to the destination
    // 2. Draw the polyline through all routes
    // 3. Scale the map to fit the bounds between current location and the destination
    func drawRouteToDestination(place : GMSPlace) -> Void {
        
        var origin:String = ""
        if (currentCoordinate != nil) {
            origin = "\(currentCoordinate.latitude),"+"\(currentCoordinate.longitude)" as String
        }
        let destination = "\(place.coordinate.latitude)," + "\(place.coordinate.longitude)"
        
        let host:String = "https://maps.googleapis.com/maps/api/directions/json"
        let parameters:String = "?origin=\(origin)&destination=\(destination)&key=\(GoogleMapsAPIKey)"
        var requestString:String = host+parameters
        requestString = requestString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        let url:NSURL = NSURL(string: requestString)!
        let urlRequest:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "GET"
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest) { (data, response, error) in
            
            var json: NSDictionary!
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as? NSDictionary
            } catch {
                print(error)
            }
            
            // NOTICE: Parse returned data from API call
            if let routesArray:NSArray = json["routes"] as? NSArray {
                if (routesArray.count>0){
                    let routeDict:NSDictionary = routesArray.objectAtIndex(0) as! NSDictionary
                    let routeOverviewPolyline = routeDict["overview_polyline"] as! NSDictionary
                    let points:String = routeOverviewPolyline["points"] as! String
                    if let polylinePath:GMSPath = GMSPath(fromEncodedPath: points)!{
                        
                        // Clear the previous polyline and draw the new one
                        if ((self.polyline) != nil) {
                            self.polyline!.map = nil
                        }
                        self.polyline = GMSPolyline(path: polylinePath)
                        self.polyline?.geodesic = true
                        self.polyline!.strokeWidth = 5
                        self.polyline!.strokeColor = UIColor.orangeColor()
                        self.polyline!.map = self.mapView
                        
                        // Scale the map
                        var bounds = GMSCoordinateBounds()
                        for index in 1...polylinePath.count() {
                            bounds = bounds.includingCoordinate(polylinePath.coordinateAtIndex(index))
                        }
                        let camera = self.mapView.cameraForBounds(bounds, insets:UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25))
                        self.mapView.camera = camera!
                    }
                }
            }
            
        }
        task.resume()
    }
    
    // MARK: <--- TextField Delegate -->
    // When user tap on the field, bring him to the search screen
    func textFieldDidBeginEditing(textField: UITextField) {
        self.performSegueWithIdentifier("goToSearchView", sender: self)
    }
    
    // MARK: <-- LocationManager Delegate -->
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager?.startUpdatingLocation()
            
            mapView?.myLocationEnabled = true
            mapView?.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let myLocation = locations.first {
            mapView?.camera = GMSCameraPosition(target: myLocation.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            currentCoordinate = myLocation.coordinate
            locationManager?.stopUpdatingLocation()
        }
    }
    
    // MARK: Other functions
    func goToAppSettings() -> Void {
        let application = UIApplication.sharedApplication()
        let settingsURL = NSURL(string: UIApplicationOpenSettingsURLString)
        
        if application .canOpenURL(settingsURL!) {
            application .openURL(settingsURL!)
        }
    }
    
    func deleteDestinationPlace(sender:UIButton) -> Void {
        
        // Clear the destination
        destinationPlace = nil
        
        // Clear search field
        searchTextField.text = ""
        
        // Hide route button
        routeButton.hidden = true
        
        // Clear destination marker
        // Clear all polylines
        mapView.clear()
        
        // Move back to current location
        mapView.animateToLocation(currentCoordinate)
    }
    
    @IBAction func routeButtonDidPress(sender: AnyObject) {
        self.drawRouteToDestination(destinationPlace!)
    }
}

