//
//  SearchLocationTextField.swift
//  FindingNemo
//
//  Created by The Son    on 7/25/16.
//  Copyright © 2016 Savvycom. All rights reserved.
//

import UIKit

class SearchLocationTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupStyle() -> Void {
        
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        self.layer.shadowColor = UIColor.clearColor().CGColor
        self.layer.shadowPath = nil
        
        self.layer.shadowRadius  = 1.5;
        self.layer.shadowColor = UIColor.lightGrayColor().CGColor;
        self.layer.shadowOffset  = CGSizeMake(0, 0);
        self.layer.shadowOpacity = 0.9;
        self.layer.masksToBounds = false;
        
        let shadowInsets:UIEdgeInsets = UIEdgeInsetsMake(0, 0, -1.5, 0)
        let shadowPath:UIBezierPath = UIBezierPath(rect: UIEdgeInsetsInsetRect(rect, shadowInsets))
        self.layer.shadowPath = shadowPath.CGPath
    }
}
    
