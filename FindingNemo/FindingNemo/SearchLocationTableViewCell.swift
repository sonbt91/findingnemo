//
//  SearchLocationTableViewCell.swift
//  FindingNemo
//
//  Created by The Son    on 7/25/16.
//  Copyright © 2016 Savvycom. All rights reserved.
//

import UIKit

class SearchLocationTableViewCell: UITableViewCell {

    @IBOutlet var locationImageView:UIImageView!
    @IBOutlet var locationPrimaryNameLabel:UILabel!
    @IBOutlet var locationSecondaryNameLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
